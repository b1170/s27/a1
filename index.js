const http = require('http');
const port = 3000;

http.createServer((req, res) => {
    if (req.url === '/' & req.method === "GET") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write('Welcome to booking system');
        res.end();
    } else if (req.url === '/profile' && req.method === "GET") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write('Welcome to your profile');
        res.end();
    } else if (req.url === '/courses' && req.method === "GET") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write(`Here's our courses available`);
        res.end();
    } else if (req.url === '/addCourse' && req.method === "POST") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write('Add course to our resources');
        res.end();
    } else if (req.url === '/updateCourse' && req.method === "PUT") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write('Updaet a course to our resources');
        res.end();
    } else if (req.url === '/archiveCourse' && req.method === "DELETE") {
        res.writeHead(200, {'Console-type': 'text/plain'})
        res.write('Archive courses to our resources');
        res.end();
    }
}).listen(port);